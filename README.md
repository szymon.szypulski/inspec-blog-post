# Running a demo

Vagrant and virtualbox have to be installed before running demo.

```
bundle install
kitchen verify
```

For more, visit https://blog.ragnarson.com.
