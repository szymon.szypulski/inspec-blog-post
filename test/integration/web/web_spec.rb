describe package('nginx') do
  it { should be_installed }
end

describe service('nginx') do
  it { should be_running }
  # it { should be_monitored.by("monit") }
end

describe file('/etc/nginx/nginx.conf') do
  it { should exist }
  its(:content) { should match(/ssl_protocols TLSv1 TLSv1.1 TLSv1.2;/) }
end

# CVE-2016-1247
describe file('/var/log/nginx') do
  it { should_not be_owned_by('www-data') }
end
