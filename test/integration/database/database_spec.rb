describe package('postgresql') do
  it { should be_installed }
end

describe service('postgresql') do
  it { should be_running }
end

describe postgres_conf do
  its('max_connections') { should eq '100' }
end

describe port(5432) do
  it { should be_listening }
  its('addresses') { should_not include '0.0.0.0' }
  its('protocols') { should include('tcp') }
end

describe port.where { protocol =~ /tcp/ && port > 22 && port < 80 } do
  it { should_not be_listening }
end
